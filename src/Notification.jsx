import React, { useContext } from 'react';
import Alert from '@material-ui/lab/Alert';
import Slide from '@material-ui/core/Slide';
import Snackbar from '@material-ui/core/Snackbar';
import { NotificationContext } from './Providers/NotificationProvider';

const Notification = () => {
  const { notification, clearNotification } = useContext(NotificationContext);
  return (
    <Snackbar
      open={!!notification.message}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      TransitionComponent={Slide}
    >
      <Alert onClose={clearNotification} severity={notification.severity}>
        {notification.message}
      </Alert>
    </Snackbar>
  );
};

export default Notification;
