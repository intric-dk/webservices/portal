import React, { createContext, useContext, useState, useEffect } from 'react';
import { AuthenticationContext } from './AuthenticationProvider';
import { getMe } from '../api';

const defaultState = {
  data: null,
  error: '',
};

export const MeContext = createContext(defaultState);

export const useMe = () => {
  const { authentication } = useContext(AuthenticationContext);
  const { me, setMe } = useContext(MeContext);

  useEffect(() => {
    if (authentication.bearer) {
      if (!me.data) {
        (async () => {
          const res = await getMe();
          setMe(res);
        })();
      }
    } else {
      setMe(defaultState);
    }
  }, [authentication, setMe, me.data]);

  return me;
};

const MeProvider = ({ children }) => {
  const [me, setMe] = useState(defaultState);
  const value = {
    me,
    setMe,
  };
  return <MeContext.Provider value={value}>{children}</MeContext.Provider>;
};

export default MeProvider;
