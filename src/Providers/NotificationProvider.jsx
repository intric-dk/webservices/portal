import React, { createContext, useState } from 'react';

const defaultState = {
  message: '',
  severity: 'error',
};

export const NotificationContext = createContext(defaultState);

const NotificationProvider = ({ children }) => {
  const [notification, setNotification] = useState(defaultState);
  const clearNotification = () => setNotification(defaultState);
  const value = {
    notification,
    setNotification,
    clearNotification,
  };
  return (
    <NotificationContext.Provider value={value}>
      {children}
    </NotificationContext.Provider>
  );
};

export default NotificationProvider;
