import React, { createContext, useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { set, get } from '../persistence';

const defaultState = {
  organisationId: '',
  bearer: '',
};

export const AuthenticationContext = createContext(defaultState);
export const AuthenticationPersistence = 'authentication';

// Retrieve the authentication information of the user from the state
// or a cookie and redirect to the user dashboard if the "sendToDashboard"
// option is set to true to avoid an unnecessary relogin.
export const useAuthentication = (callback) => {
  const { replace } = useHistory();
  const { organisationSlug } = useParams();
  const { authentication, setAuthentication } = useContext(
    AuthenticationContext
  );

  useEffect(() => {
    // Check if authentication is available in state.
    if (!authentication.bearer) {
      // Load authentication from persistence layer.
      const persistence = get(AuthenticationPersistence);
      if (persistence && persistence.bearer) {
        // Update global authentication state from persisted information.
        return setAuthentication(persistence);
      }

      // Redirect user if unauthenticated.
      if (organisationSlug) {
        // Redirect user to organisation login page instead of general login page.
        return replace(`/${organisationSlug}`);
      }
      return replace('/');
    }
  }, [organisationSlug, authentication, setAuthentication, callback, replace]);

  return authentication;
};

// Redirect to dashboard if user is already logged in.
export const useLoginSkip = () => {
  const authentication = useAuthentication();
  const { replace } = useHistory();

  useEffect(() => {
    if (authentication.organisationId) {
      replace(`/${authentication.organisationId}/-/dashboard`);
    }
  }, [authentication, replace]);
};

const AuthenticationProvider = ({ children }) => {
  const [authentication, setAuthentication] = useState(defaultState);
  const setAll = (state) => {
    setAuthentication(state);
    set(AuthenticationPersistence, state);
  };
  const value = {
    authentication,
    setAuthentication: (state) => setAll(state),
    clearAuthentication: () => setAll(defaultState),
  };
  return (
    <AuthenticationContext.Provider value={value}>
      {children}
    </AuthenticationContext.Provider>
  );
};

export default AuthenticationProvider;
