import React from 'react';
import AuthenticationProvider from './AuthenticationProvider';
import NotificationProvider from './NotificationProvider';
import MeProvider from './MeProvider';

const CombinedStateProvider = ({ children }) => (
  <AuthenticationProvider>
    <NotificationProvider>
      <MeProvider>{children}</MeProvider>
    </NotificationProvider>
  </AuthenticationProvider>
);

export default CombinedStateProvider;
