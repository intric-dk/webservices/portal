import React from 'react';
import { useAuthentication } from './Providers/AuthenticationProvider';

const AuthN = (UnprotectedComponent) => {
  const ProtectedComponent = () => {
    const authentication = useAuthentication();
    return authentication.bearer ? <UnprotectedComponent /> : null;
  };

  return ProtectedComponent;
};

export default AuthN;
