import React, { useState, useEffect } from 'react';
import Alert from '@material-ui/lab/Alert';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import { useAuthentication } from '../Providers/AuthenticationProvider';
import { getOrganisations } from '../api';
import { CardContent } from '@material-ui/core';

const defaultState = { data: null, error: '', loading: false };

const useStyles = makeStyles((theme) => ({
  result: {
    minHeight: theme.spacing(2),
  },
  spinner: {
    width: '100%',
  },
}));

const OrganisationInfo = () => {
  const classes = useStyles();
  const authentication = useAuthentication();
  const [organisation, setOrganisation] = useState(defaultState);

  const loadOrganisation = () => {
    if (authentication.organisationId) {
      (async () => {
        setOrganisation((state) => ({ ...state, loading: true }));
        const res = await getOrganisations(authentication.organisationId);
        setOrganisation((state) => ({ ...state, ...res, loading: false }));
      })();
    }
  };

  useEffect(loadOrganisation, [authentication]);

  return (
    <Grid spacing={2} container>
      <Grid xs={12} item>
        <Card variant="outlined">
          <CardHeader
            title="About this organisation"
            titleTypographyProps={{ variant: 'h6' }}
          ></CardHeader>
          <CardContent>
            {organisation.data ? (
              <List className={classes.result}>
                <ListItem>
                  <ListItemAvatar>
                    <Avatar
                      variant="rounded"
                      src={organisation.data.avatar_url}
                    ></Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={organisation.data.name}
                    secondary={organisation.data.description}
                  />
                </ListItem>
              </List>
            ) : organisation.loading ? (
              <Grid
                className={classes.results}
                justify="center"
                alignItems="center"
                container
              >
                <Grid item>
                  <CircularProgress
                    className={classes.spinner}
                  ></CircularProgress>
                </Grid>
              </Grid>
            ) : (
              <Alert
                severity="error"
                action={
                  <Button
                    onClick={loadOrganisation}
                    color="inherit"
                    variant="outlined"
                    size="small"
                  >
                    Retry
                  </Button>
                }
              >
                {organisation.error}
              </Alert>
            )}
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default OrganisationInfo;
