import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Alert from '@material-ui/lab/Alert';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import { useAuthentication } from '../Providers/AuthenticationProvider';
import { findOrganisationsProjects } from '../api';

const defaultState = { data: [], error: '', loading: false };

const resultCount = 1;
const useStyles = makeStyles((theme) => ({
  results: {
    minHeight: theme.spacing(2) + 72 * resultCount,
    overflow: 'auto',
  },
  spinner: {
    width: '100%',
  },
}));

const ProjectList = () => {
  const classes = useStyles();
  const authentication = useAuthentication();
  const [projects, setProjects] = useState(defaultState);

  const loadOrganisationsProjects = () => {
    if (authentication.organisationId) {
      (async () => {
        setProjects((state) => ({ ...state, loading: true }));
        const res = await findOrganisationsProjects(
          authentication.organisationId
        );
        setProjects((state) => ({ ...state, ...res, loading: false }));
      })();
    }
  };

  useEffect(loadOrganisationsProjects, [authentication]);

  return (
    <Grid spacing={2} container>
      <Grid xs={12} item>
        <Card variant="outlined">
          <CardHeader
            title="Projects"
            titleTypographyProps={{ variant: 'h6' }}
          ></CardHeader>
          <CardContent>
            {projects.data.length ? (
              <List className={classes.results}>
                {projects.data.map((project) => (
                  <ListItem key={project.id} component={Link} to="#" button>
                    <ListItemAvatar>
                      <Avatar
                        variant="rounded"
                        src={project.avatar_url}
                      ></Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={project.name}
                      secondary={project.description}
                    />
                  </ListItem>
                ))}
              </List>
            ) : projects.loading ? (
              <Grid
                className={classes.results}
                justify="center"
                alignItems="center"
                container
              >
                <Grid item>
                  <CircularProgress
                    className={classes.spinner}
                  ></CircularProgress>
                </Grid>
              </Grid>
            ) : !projects.error ? (
              <Alert
                severity="info"
                action={
                  <Button color="inherit" variant="outlined" size="small">
                    Create first project
                  </Button>
                }
              >
                No projects found!
              </Alert>
            ) : (
              <Alert
                severity="error"
                action={
                  <Button
                    onClick={loadOrganisationsProjects}
                    color="inherit"
                    variant="outlined"
                    size="small"
                  >
                    Retry
                  </Button>
                }
              >
                {projects.error}
              </Alert>
            )}
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default ProjectList;
