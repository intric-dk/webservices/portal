import React from 'react';
import { useParams } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Menu from '../Menu';
import GreetingMessage from './GreetingMessage';
import OrganisationInfo from './OrganisationInfo';
import ProjectList from './ProjectList';
import { useOrganisationSlug } from '../hooks';

const useStyles = makeStyles((theme) => ({
  grid: {
    padding: theme.spacing(),
    margin: 0,
    width: '100%',
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const { organisationSlug } = useParams();

  // Replace organisation ID with slug for easier paths.
  useOrganisationSlug();

  return (
    <>
      <Menu></Menu>
      <Grid justify="center" container>
        <Grid
          className={classes.grid}
          xs={12}
          sm={10}
          md={8}
          lg={6}
          xl={6}
          spacing={2}
          direction="column"
          item
          container
        >
          <Grid item>
            <GreetingMessage></GreetingMessage>
          </Grid>
          {/* TODO: Add info about what it means if you are logged in as guest! Users can only create organisations! */}
          {organisationSlug !== 'guest' && (
            <Grid item>
              <OrganisationInfo></OrganisationInfo>
            </Grid>
          )}
          {organisationSlug !== 'guest' && (
            <Grid item>
              <ProjectList></ProjectList>
            </Grid>
          )}
        </Grid>
      </Grid>
    </>
  );
};

export default Dashboard;
