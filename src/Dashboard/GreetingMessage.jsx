import React, { useMemo } from 'react';
import Alert from '@material-ui/lab/Alert';
import { useMe } from '../Providers/MeProvider';

const greetings = [
  'Hey',
  'Hello',
  `What's up`,
  'Good to see you',
  'Nice to see you',
  'Welcome',
];

const GreetingMessage = () => {
  const me = useMe();
  const greeting = useMemo(
    () => greetings[Math.floor(Math.random() * greetings.length)],
    []
  );

  return (
    <Alert severity="info">
      {me.data ? `${greeting}, ${me.data.first_name}!` : `${greeting}!`}
    </Alert>
  );
};

export default GreetingMessage;
