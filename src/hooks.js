import { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { getOrganisations } from './api';

// Replace the organisation ID with the slug for
// more human-readable paths.
export const useOrganisationSlug = () => {
  const { replace, location } = useHistory();
  const { organisationSlug } = useParams();

  useEffect(() => {
    if (organisationSlug && organisationSlug !== 'guest') {
      (async () => {
        // Fetch organisation.
        const res = await getOrganisations(organisationSlug);
        // Check if the organisation slug is the ID of the entity.
        if (res.data && res.data.id === organisationSlug) {
          // Replace the ID with the slug for easier paths.
          return replace(location.pathname.replace(res.data.id, res.data.slug));
        }
      })();
    }
  }, [organisationSlug, replace, location.pathname]);
};
