import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import CenteredLayout from './CenteredLayout';
import GuestLoginButton from './GuestLoginButton';
import { useLoginSkip } from '../Providers/AuthenticationProvider';
import { findOrganisations } from '../api';

const resultCount = 3;
const useStyles = makeStyles((theme) => ({
  results: {
    height: theme.spacing(2) + 72 * resultCount,
    overflow: 'auto',
  },
  spinner: {
    width: '100%',
  },
}));

const Home = () => {
  const classes = useStyles();

  // Search logic.
  const [keyword, setKeyword] = useState('');
  const [organisations, setOrganisations] = useState({ data: [], error: null });
  const onSearch = async (e) => {
    const { value } = e.target;

    // Update textfield state.
    setKeyword(value);

    // Call API.
    const res = await findOrganisations({ q: value.toLowerCase() });
    setOrganisations(res);
  };

  useEffect(() => {
    // Prevent state updates on unmounted component.
    let isMounted = true;

    // Call API.
    (async () => {
      const res = await findOrganisations({ q: '' });
      if (isMounted) {
        setOrganisations(res);
      }
    })();

    return () => {
      isMounted = false;
    };
  }, []);

  // Redirect to dashboard if user is already logged in.
  useLoginSkip();

  return (
    <CenteredLayout>
      <CardHeader
        title="Intric&nbsp;Portal"
        titleTypographyProps={{ variant: 'h6' }}
        subheader="Boosting hackerspaces and makerplaces."
      ></CardHeader>
      <CardContent>
        <Grid spacing={2} container>
          <Grid xs={12} item>
            <TextField
              label="Search an organisation to sign in"
              placeholder="Intric"
              variant="outlined"
              onChange={onSearch}
              value={keyword}
              fullWidth
            ></TextField>
          </Grid>
          <Grid xs={12} item>
            {organisations.data.length ? (
              <List className={classes.results}>
                {organisations.data.map((organisation) => (
                  <ListItem
                    key={organisation.id}
                    component={Link}
                    to={`/${organisation.slug}`}
                    button
                  >
                    <ListItemAvatar>
                      <Avatar
                        variant="rounded"
                        src={organisation.avatar_url}
                      ></Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={organisation.name}
                      secondary={organisation.description}
                      secondaryTypographyProps={{
                        noWrap: true,
                        display: 'block',
                      }}
                    />
                  </ListItem>
                ))}
              </List>
            ) : (
              <Grid
                className={classes.results}
                justify="center"
                alignItems="center"
                container
              >
                <Grid item>
                  {keyword.length ? (
                    <Typography align="center">
                      {organisations.error || 'No matching organisation found.'}
                    </Typography>
                  ) : (
                    <CircularProgress
                      className={classes.spinner}
                    ></CircularProgress>
                  )}
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>
      </CardContent>
      <CardActions>
        <Grid spacing={1} container>
          <Grid xs={12} md={6} item>
            <GuestLoginButton></GuestLoginButton>
          </Grid>
        </Grid>
      </CardActions>
    </CenteredLayout>
  );
};

export default Home;
