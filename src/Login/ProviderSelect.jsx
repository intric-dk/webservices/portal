import React, { useState, useEffect, useContext } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CenteredLayout from './CenteredLayout';
import GuestLoginButton from './GuestLoginButton';
import { NotificationContext } from '../Providers/NotificationProvider';
import { useLoginSkip } from '../Providers/AuthenticationProvider';
import { getOrganisations, findAuthenticationProviders, authUrl } from '../api';
import { useOrganisationSlug } from '../hooks';

const resultCount = 2;
const useStyles = makeStyles((theme) => ({
  results: {
    height: theme.spacing(2) + 72 * resultCount,
    overflow: 'auto',
  },
  spinner: {
    width: '100%',
  },
}));

const ProviderSelect = () => {
  const classes = useStyles();
  const { organisationSlug } = useParams();
  const { replace } = useHistory();
  const { setNotification } = useContext(NotificationContext);
  const [organisation, setOrganisation] = useState({ data: null, error: '' });
  const [authenticationProviders, setAuthenticationProviders] = useState({
    data: [],
    error: '',
  });
  const isGuest = organisationSlug === 'guest';
  useEffect(() => {
    // Prevent state updates on unmounted component.
    let isMounted = true;

    // Call API.
    (async () => {
      if (!isGuest) {
        const res = await getOrganisations(organisationSlug);

        // Check if the organisation was found.
        if (!res.data) {
          replace('/');
          return setNotification({
            message: `We couldn't find your organisation!`,
            severity: 'error',
          });
        }

        if (isMounted) {
          return setOrganisation(res);
        }
      }

      if (isMounted) {
        return setOrganisation({ data: null, error: '' });
      }
    })();
    (async () => {
      const res = await findAuthenticationProviders();

      if (isMounted) {
        // Only display global authentication providers.
        res.data = res.data.filter((item) => item.global);
        setAuthenticationProviders(res);
      }
    })();

    return () => {
      isMounted = false;
    };
  }, [replace, isGuest, organisationSlug, setNotification]);

  // Redirect to dashboard if user is already logged in.
  useLoginSkip();

  // Replace organisation ID with slug for easier paths.
  useOrganisationSlug();

  const getHeader = () => {
    if (isGuest) {
      return 'Sign in as guest';
    }
    if (organisation.data && organisation.data.name) {
      return `Sign in to ${organisation.data.name}`;
    }
    return 'Sign in';
  };

  return (
    <CenteredLayout>
      <CardHeader
        avatar={
          <Avatar
            variant="rounded"
            src={organisation.data ? organisation.data.avatar_url : undefined}
          ></Avatar>
        }
        title={getHeader()}
        titleTypographyProps={{ variant: 'h6' }}
        subheader="How do you want to sign in?"
      ></CardHeader>
      <CardContent>
        {authenticationProviders.data.length ? (
          <List className={classes.results}>
            {authenticationProviders.data.map((authenticationProvider) => (
              <ListItem
                key={authenticationProvider.id}
                component="a"
                href={
                  organisation.data
                    ? authUrl(authenticationProvider.id, organisation.data.id)
                    : authUrl(authenticationProvider.id, 'guest')
                }
                button
              >
                <ListItemAvatar>
                  <Avatar
                    variant="rounded"
                    src={`/img/logo_${authenticationProvider.provider}.svg`}
                  ></Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={authenticationProvider.name}
                  secondary={`Sign in with ${authenticationProvider.name}`}
                />
              </ListItem>
            ))}
          </List>
        ) : (
          <Grid
            className={classes.results}
            justify="center"
            alignItems="center"
            container
          >
            <Grid item>
              <Typography align="center">
                {authenticationProviders.error ||
                  'No authentication provider found.'}
              </Typography>
            </Grid>
          </Grid>
        )}
      </CardContent>
      <CardActions>
        <Grid spacing={1} container>
          {!isGuest ? (
            <Grid xs={12} md={6} item>
              <GuestLoginButton></GuestLoginButton>
            </Grid>
          ) : null}
          <Grid xs={12} md={6} item>
            <Button
              component="a"
              href="/"
              variant="outlined"
              color="primary"
              fullWidth
            >
              Use another organisation
            </Button>
          </Grid>
        </Grid>
      </CardActions>
    </CenteredLayout>
  );
};

export default ProviderSelect;
