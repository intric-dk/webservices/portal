import React from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  grid: {
    minHeight: '100vh',
    padding: theme.spacing(1),
  },
}));

const CenteredLayout = ({ children }) => {
  const classes = useStyles();
  return (
    <Grid
      className={classes.grid}
      justify="center"
      alignItems="center"
      container
    >
      <Grid xs={12} sm={10} md={8} lg={6} xl={4} item>
        <Card variant="outlined">{children}</Card>
      </Grid>
    </Grid>
  );
};

export default CenteredLayout;
