import React, { useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import { AuthenticationContext } from '../Providers/AuthenticationProvider';
import { parseHash } from '../api';
import { Grid } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  grid: {
    minHeight: '100vh',
    padding: theme.spacing(1),
  },
  spinner: {
    width: '100%',
  },
}));

const Login = () => {
  const classes = useStyles();
  const { replace, location } = useHistory();
  const { authentication, setAuthentication } = useContext(
    AuthenticationContext
  );
  useEffect(() => {
    // Check if authentication is available in state.
    if (authentication.bearer && authentication.organisationId) {
      return replace(`/${authentication.organisationId}/dashboard`);
    }

    // Load authentication state from hash fragment.
    if (location.hash) {
      const data = parseHash(location.hash);

      if (data.organisation_id && data.bearer) {
        setAuthentication({
          organisationId: data.organisation_id,
          bearer: data.bearer,
        });
        return replace(`/${data.organisation_id}/-/dashboard`);
      }
    }

    return replace('/');
  }, [authentication, setAuthentication, location, replace]);
  return (
    <Grid
      className={classes.grid}
      justify="center"
      alignItems="center"
      container
    >
      <Grid item>
        <CircularProgress className={classes.spinner}></CircularProgress>
      </Grid>
    </Grid>
  );
};

export default Login;
