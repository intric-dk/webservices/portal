import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const GuestLoginButton = () => (
  <Button
    component={Link}
    to="/guest"
    variant="outlined"
    color="primary"
    fullWidth
  >
    Sign in as guest
  </Button>
);

export default GuestLoginButton;
