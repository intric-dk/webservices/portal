import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import CombinedStateProvider from './Providers/CombinedStateProvider';
import Notification from './Notification';
import Home from './Login/Home';
import Login from './Login/Login';
import Dashboard from './Dashboard/Dashboard';
import ProviderSelect from './Login/ProviderSelect';
import AuthN from './AuthN';
import theme from './theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
    <CssBaseline />
    <CombinedStateProvider>
      <Notification />
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/-/login" component={Login} />
          <Route
            path="/:organisationSlug/-/dashboard"
            component={AuthN(Dashboard)}
          />
          <Route path="/:organisationSlug" component={ProviderSelect} />
          <Redirect to="/" />
        </Switch>
      </BrowserRouter>
    </CombinedStateProvider>
  </ThemeProvider>,
  document.querySelector('#root')
);
