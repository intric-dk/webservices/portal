import React, { useState, useContext } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Popover from '@material-ui/core/Popover';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { AuthenticationContext } from './Providers/AuthenticationProvider';
import { useMe } from './Providers/MeProvider';

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
  },
  iconButton: {
    padding: 0,
  },
  avatar: {
    border: `2px solid ${theme.palette.background.paper}`,
    margin: theme.spacing(),
    width: theme.spacing(6),
    height: theme.spacing(6),
  },
}));

const Menu = () => {
  const classes = useStyles();
  const [anchor, setAnchor] = useState(null);
  const { clearAuthentication } = useContext(AuthenticationContext);
  const me = useMe();

  const onClick = (e) => setAnchor(e.currentTarget);
  const onClose = () => setAnchor(null);
  const open = !!anchor;

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography component="h1" variant="h6" className={classes.title}>
          Intric&nbsp;Portal
        </Typography>
        {me.data && (
          <>
            <IconButton className={classes.iconButton} onClick={onClick}>
              <Avatar
                className={classes.avatar}
                src={me.data.avatar_url}
              ></Avatar>
            </IconButton>
            <Popover
              open={open}
              anchorEl={anchor}
              onClose={onClose}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            >
              <List>
                <ListItem>
                  <ListItemText
                    primary={`${me.data.first_name} ${me.data.last_name}`}
                    secondary={me.data.email}
                  ></ListItemText>
                </ListItem>
                <Divider></Divider>
                <ListItem onClick={clearAuthentication} button>
                  <ListItemText>Logout</ListItemText>
                </ListItem>
              </List>
            </Popover>
          </>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Menu;
