import { get } from './persistence';
import { AuthenticationPersistence } from './Providers/AuthenticationProvider';

const apiUrl = process.env.REACT_APP_API_URL;
const appUrl = process.env.REACT_APP_APP_URL;

const toQueryString = (queryParameters) => {
  const queryParameterKeys = Object.keys(queryParameters);
  return queryParameterKeys.length
    ? Object.keys(queryParameters).reduce((total, key, index) => {
        const value = encodeURIComponent(queryParameters[key]);

        // Don't append empty values.
        if (!value) {
          return total;
        }

        const segment = `${key}=${value}`;

        // Check if this is the last segment.
        if (index === queryParameterKeys.length - 1) {
          return `${total}${segment}`;
        }

        return `${total}${segment}&`;
      }, '?')
    : '';
};

const callApi = async (verb, path, body, queryParameters = {}) => {
  // Check if the call returns a list.
  const returnsList = verb === 'FIND';
  const emtpyPayload = returnsList ? [] : null;

  // Create request config.
  const config = { method: returnsList ? 'GET' : verb };
  if (body) {
    config.body = JSON.stringify(body);
  }

  // Get authentication token from persistence layer.
  const persistence = get(AuthenticationPersistence);
  if (persistence && persistence.bearer) {
    if (!config.headers) {
      config.headers = {};
    }
    config.headers.Authorization = `Bearer ${persistence.bearer}`;
  }

  // Assemble query string.
  const queryString = toQueryString(queryParameters);

  // Perform HTTP request.
  try {
    const res = await fetch(`${apiUrl}${path}${queryString}`, config);
    const payload = await res.json();
    if (res.status === 204) {
      return { data: null, error: '' };
    }
    if (res.status <= 300) {
      return { data: payload.data || emtpyPayload, error: '' };
    }
    return { data: emtpyPayload, error: payload.error.reason };
  } catch (e) {
    if (!e.response) {
      return { data: emtpyPayload, error: 'Loading resource failed.' };
    }
    return { data: emtpyPayload, error: e.response.error.reason };
  }
};

export const parseHash = (hash) =>
  hash
    .substring(1)
    .split(/&/g)
    .reduce((total, current) => {
      const [key, value] = current.split('=');
      return {
        ...total,
        [key]: value,
      };
    }, {});

export const findOrganisations = (queryParameters = {}) =>
  callApi('FIND', '/v1/organisations', null, queryParameters);

export const getOrganisations = (id, queryParameters = {}) =>
  callApi('GET', `/v1/organisations/${id}`, null, queryParameters);

export const findAuthenticationProviders = (queryParameters = {}) =>
  callApi('FIND', '/v1/authentication_providers', null, queryParameters);

export const findOrganisationsProjects = (id, queryParameters = {}) =>
  callApi('FIND', `/v1/organisations/${id}/projects`, null, queryParameters);

export const getMe = () => callApi('GET', `/v1/me`, null, {});

export const authUrl = (providerId, organisationId) =>
  `${apiUrl}/v1/authentication_providers/${providerId}/redirects/login?redirect_uri=${appUrl}/-/login&organisation_id=${organisationId}`;
