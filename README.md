# Portal

A frontend for the Intric platform.

## Development

Make sure to have the latest LTS of NodeJS installed. Now create a `.env` file:

```ini
REACT_APP_API_URL=https://api.intric.dk
```

Afterwards install the dependencies and run the development server:

```shell
$ npm ci
$ npm start
```

## Deployment

The application can be built by running `npm run build`. The webserver should handle `404 - Not Found` errors by serving the `index.html`.

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
